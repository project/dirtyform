# Dirty Form

Dirty form detection. Alerts users to unsaved changes if they attempt to close 
the browser or navigate away from the page.

Uses the [jquery.AreYouSure](https://github.com/codedance/jquery.AreYouSure) 
library.

## Installation

[Download jquery.AreYouSure](https://github.com/codedance/jquery.AreYouSure/releases)
and [install](https://www.drupal.org/node/1440066).

Enable this and the [Libraries API](https://www.drupal.org/project/libraries) 
modules.

## Configuration

By default node edit forms are enabled. To configure forms to enable, go to
admin/config/user-interface/dirtyform.
