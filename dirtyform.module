<?php

/**
 * @file
 * Main module file.
 */

/**
 * Implements hook_menu().
 */
function dirtyform_menu() {
  $items = array();

  $items['admin/config/user-interface/dirtyform'] = array(
    'title' => 'Dirty Form settings',
    'description' => 'Javascript detection of modified form elements.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('dirtyform_config_form'),
    'access arguments' => array('administer site configuration'),
    'type' => MENU_NORMAL_ITEM,
    'file' => 'dirtyform.admin.inc',
  );

  return $items;
}

/**
 * Implements hook_libraries_info().
 */
function dirtyform_libraries_info() {
  $libraries['jquery.AreYouSure'] = array(
    'name' => 'Are You Sure? JQuery Plugin',
    'vendor url' => 'https://github.com/codedance/jquery.AreYouSure',
    'download url' => 'https://github.com/codedance/jquery.AreYouSure/releases',
    'version callback' => 'dirtyform_library_version',
    'files' => array(
      'js' => array('jquery.are-you-sure.js'),
    ),
  );
  return $libraries;
}

/**
 * Gets installed jquery.AreYouSure version.
 *
 * @see dirtyform_libraries_info()
 */
function dirtyform_library_version($library) {
  if ($path = libraries_get_path('jquery.AreYouSure', TRUE)) {
    if ($s = file_get_contents(DRUPAL_ROOT . '/' . $path . '/package.json')) {
      $json = drupal_json_decode($s);
      if (isset($json['version'])) {
        return $json['version'];
      }
    }
  }
  return NULL;
}

/**
 * Implements hook_form_alter().
 */
function dirtyform_form_alter(&$form, &$form_state, $form_id) {
  if (($library = libraries_detect('jquery.AreYouSure')) &&
      !empty($library['installed'])
  ) {
    if (dirtyform_form_id($form_id)) {
      $form['#attached']['libraries_load'][] = array('jquery.AreYouSure');
      $conf = array(
        'addRemoveFieldsMarksDirty' => TRUE,
      );
      drupal_alter('dirtyform_configure', $conf, $form);
      drupal_add_js(array('dirtyform' => array($form['#id'] => $conf)), 'setting');
      drupal_add_js(drupal_get_path('module', 'dirtyform') . '/dirtyform.js');
    }
  }
  else {
    watchdog('dirtyform', 'jquery.AreYouSure library not found.', array(),
      WATCHDOG_ERROR);
  }
}

/**
 * Determine if dirtyform is enabled for the form id.
 */
function dirtyform_form_id($form_id) {
  if (in_array($form_id, dirtyform_form_id_whitelist())) {
    return TRUE;
  }
  if (in_array($form_id, dirtyform_form_id_blacklist())) {
    return FALSE;
  }
  foreach (dirtyform_form_id_regex() as $pattern) {
    if (preg_match($pattern, $form_id)) {
      return TRUE;
    }
  }
  return FALSE;
}

/**
 * Getter/setter for the configured form id whitelist.
 */
function dirtyform_form_id_whitelist($value = NULL) {
  if (isset($value)) {
    variable_set('dirtyform_form_id_whitelist', $value);
  }
  return variable_get('dirtyform_form_id_whitelist', array());
}

/**
 * Getter/setter for the configured form id blacklist.
 */
function dirtyform_form_id_blacklist($value = NULL) {
  if (isset($value)) {
    variable_set('dirtyform_form_id_blacklist', $value);
  }
  return variable_get('dirtyform_form_id_blacklist', array());
}

/**
 * Getter/setter for the configured form id regex.
 */
function dirtyform_form_id_regex($value = NULL) {
  if (isset($value)) {
    variable_set('dirtyform_form_id_regex', $value);
  }
  return variable_get('dirtyform_form_id_regex', array('/.*_node_form/'));
}
